package me.elucent.earlgray.props;

import me.elucent.earlgray.api.Trait;
import net.minecraft.nbt.CompoundTag;

public class IntTrait extends Trait {
    int value = 0;

    @Override
    public CompoundTag write(CompoundTag tag) {
        tag.putInt("value", value);
        return tag;
    }

    @Override
    public Trait read(CompoundTag tag) {
        value = tag.getInt("value");
        return this;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int i) {
        value = i;
    }
}
