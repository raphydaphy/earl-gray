package me.elucent.earlgray.mixin;

import me.elucent.earlgray.api.TraitRegistry;
import net.minecraft.entity.Entity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.function.Consumer;

@Mixin(World.class)
public class WorldMixin {
    @Inject(at = @At("HEAD"), method = "tickEntity")
    protected void onEntityAdded(Consumer<Entity> consumer, Entity entity, CallbackInfo info) {
        if (entity.age == 0) TraitRegistry.applyInherents(entity);
    }
}
