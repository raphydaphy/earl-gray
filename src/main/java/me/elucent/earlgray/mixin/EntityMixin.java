package me.elucent.earlgray.mixin;

import me.elucent.earlgray.api.TraitContainer;
import me.elucent.earlgray.api.TraitHolder;
import me.elucent.earlgray.EarlGray;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundTag;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Entity.class)
public class EntityMixin implements TraitHolder {
	protected TraitContainer traits = new TraitContainer((Entity)(Object)this);

	@Inject(at = @At("HEAD"), method = "tick()V")
	private void update(CallbackInfo info) {
		traits.update();
	}

	@Override
	public TraitContainer getTraits() {
		return traits;
	}

	@Inject(at = @At("HEAD"), method = "fromTag(Lnet/minecraft/nbt/CompoundTag;)V")
	private void fromTag(CompoundTag tag, CallbackInfo info) {
		traits.read(tag.getCompound(EarlGray.MODID + ":traitTag"));
	}

	@Inject(at = @At("HEAD"), method = "toTag(Lnet/minecraft/nbt/CompoundTag;)Lnet/minecraft/nbt/CompoundTag;")
	private void toTag(CompoundTag tag, CallbackInfoReturnable<CompoundTag> info) {
		tag.put(EarlGray.MODID + ":traitTag", traits.write());
	}
}
