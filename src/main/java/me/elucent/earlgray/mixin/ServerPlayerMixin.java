package me.elucent.earlgray.mixin;

import me.elucent.earlgray.api.Trait;
import me.elucent.earlgray.api.TraitHolder;
import me.elucent.earlgray.api.Traits;
import net.minecraft.server.network.ServerPlayerEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ServerPlayerEntity.class)
public class ServerPlayerMixin {
    @Inject(at = @At("RETURN"), method = "method_14203(Lnet/minecraft/server/network/ServerPlayerEntity;Z)V")
    public void method_14203(ServerPlayerEntity original, boolean dimchanged, CallbackInfo info) {
       if (!original.world.isClient) {
           for (Trait t : ((TraitHolder)original).getTraits().getAll()) {
               Traits.add((ServerPlayerEntity)(Object)this, t);
               if (!dimchanged) {
                   t.onRespawn((ServerPlayerEntity)(Object)this);
               }
           }
       }
    }
}
