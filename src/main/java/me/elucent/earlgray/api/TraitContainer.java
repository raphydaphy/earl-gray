package me.elucent.earlgray.api;

import me.elucent.earlgray.util.NetUtil;
import me.elucent.earlgray.EarlGray;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.util.Identifier;

import java.util.HashMap;
import java.util.Map;

public class TraitContainer {
    Map<Identifier, Trait> traits = new HashMap<>();
    Map<Identifier, Trait> ticking = new HashMap<>();
    boolean dirty = true;
    Entity entity;

    public TraitContainer(Entity entity) {
        this.entity = entity;
    }

    protected void put(Identifier id, Trait p) {
        if (p == null) return;
        p.container = this;
        traits.put(id, p);
        if (p.isTickable()) ticking.put(id, p);
    }

    public <T extends Trait> void addTrait(T trait) {
        TraitEntry<T> entry = TraitRegistry.getEntry(trait);
        if (entry != null) {
            Trait existing = getTrait(entry);
            if (existing != null) {
                existing.renew(trait);
            }
            else {
                put(entry.getName(), trait);
                trait.onAdd(entity);
            }
            dirty = true;
        }
        else {
            System.out.println("Error: failed to add trait to entity, trait class "
                    + trait.getClass().getName() + " doesn't appear to be registered!");
        }
    }

    public Iterable<Trait> getAll() {
        return traits.values();
    }

    public void update() {
        for (Trait p : ticking.values()) p.update(entity);

        if (dirty) {
            dirty = false;
            NetUtil.sendToNearby(EarlGray.getPropertyPacket(entity), entity.world, entity.getBlockPos());
        }
    }

    public CompoundTag write() {
        CompoundTag tag = new CompoundTag();
        ListTag pl = new ListTag();
        for (Identifier s : traits.keySet()) {
            pl.add(new StringTag(s.toString()));
            tag.put(s.toString(), traits.get(s).write(new CompoundTag()));
        }
        tag.put("traitList", pl);
        return tag;
    }

    public void read(CompoundTag tag) {
        traits.clear();

        ListTag pl = tag.getList("traitList", 8);

        for (int i = 0; i < pl.size(); i++) {
            Identifier id = Identifier.createSplit(pl.getString(i), ':');
            put(id, TraitRegistry.generate(id, tag.getCompound(pl.getString(i))));
        }
    }

    public <T extends Trait> T getTrait(TraitEntry<T> entry) {
        return (T)traits.get(entry.getName());
    }

    public boolean hasTrait(TraitEntry<?> entry) {
        return traits.containsKey(entry.getName());
    }

    public boolean hasTrait(Trait trait) {
        TraitEntry id = TraitRegistry.getEntry(trait);
        return id == null ? false : traits.containsKey(id.getName());
    }

    public <T extends Trait> void removeTrait(TraitEntry<T> entry) {
        Trait t = traits.get(entry.getName());
        if (t != null) t.onRemove(entity);
        traits.remove(entry.getName());
        ticking.remove(entry.getName());
        dirty = true;
    }

    public <T extends Trait> void removeTrait(T trait) {
        TraitEntry<T> entry = TraitRegistry.getEntry(trait);
        if (entry != null) removeTrait(entry);
    }

    public void markDirty() {
        dirty = true;
    }
}
