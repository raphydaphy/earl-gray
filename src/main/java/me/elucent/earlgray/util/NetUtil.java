package me.elucent.earlgray.util;

import net.minecraft.network.Packet;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BoundingBox;
import net.minecraft.world.World;

import java.util.List;

public class NetUtil {
    public static void sendToNearby(Packet p, World world, BlockPos pos) {
        sendToNearby(p, world, pos, 64);
    }

    public static void sendToNearby(Packet p, World world, BlockPos pos, int range) {
        if (world.isClient) return;
        List<ServerPlayerEntity> nearby = world.getEntities(ServerPlayerEntity.class, new BoundingBox(pos).expand(range), null);
        for (ServerPlayerEntity e : nearby) {
            e.networkHandler.sendPacket(p);
        }
    }
}
