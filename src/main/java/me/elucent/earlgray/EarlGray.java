package me.elucent.earlgray;

import io.netty.buffer.Unpooled;
import me.elucent.earlgray.api.TraitHolder;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.minecraft.client.network.packet.CustomPayloadS2CPacket;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Packet;
import net.minecraft.util.Identifier;
import net.minecraft.util.PacketByteBuf;
import net.minecraft.world.World;

public class EarlGray implements ModInitializer {
	public static final String MODID = "earlgray";

	public static final Identifier SYNC_TRAITS_PACKET = new Identifier(MODID, "sync_traits");

	@Override
	public void onInitialize() {
		System.out.println("[Earl Gray]: *sips tea*");

		ClientSidePacketRegistry.INSTANCE.register(SYNC_TRAITS_PACKET, (packetContext, packetByteBuf) -> {
			PlayerEntity p = packetContext.getPlayer();
			if (p == null) return;
			World w = p.world;
			if (w == null) return;
			Entity e = w.getEntityById(packetByteBuf.readInt());
			if (e != null) {
				CompoundTag tag = packetByteBuf.readCompoundTag();
				((TraitHolder)e).getTraits().read(tag);
			}
		});
	}

	public static Packet getPropertyPacket(Entity entity) {
		PacketByteBuf buf = new PacketByteBuf(Unpooled.buffer());
		buf.writeInt(entity.getEntityId());
		buf.writeCompoundTag(((TraitHolder)entity).getTraits().write());
		return new CustomPayloadS2CPacket(SYNC_TRAITS_PACKET, buf);
	}
}
